package br.com.alura.forum.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

public class AutenticacaoViaTokenFilter extends OncePerRequestFilter {

    private TokenService tokenService;

    /*A ideia seria injetar esse TokenService. Porém tem um problema.
     Nesse tipo de classe não conseguimos fazer injeção de dependências.
     Não dá para colocar um @AutoWired, até porque na classe de security 
     Configuration nós que instanciamos manualmente a classe.*/

    public AutenticacaoViaTokenFilter(TokenService tokenService){
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

            String token = recuperarToken(request);
            boolean valido = TokenService.isTokenValido(token);
            filterChain.doFilter(request, response);
    }

	private String recuperarToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (token == null || token.isEmpty() || token.startsWith("Bearer ")){
            return null;
        }
        return token.substring(7,token.length());
	}


}