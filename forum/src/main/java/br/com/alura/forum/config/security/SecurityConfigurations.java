package br.com.alura.forum.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.alura.forum.config.security.AutenticacaoService;

@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigurerAdapter{
    
    @Autowired
    private AutenticacaoService autenticacaoService;
   
    @Autowired
    private TokenService tokenService;

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }

    //Configuracoes de autenticacao
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(autenticacaoService).passwordEncoder(new BCryptPasswordEncoder());
    }

    // Configuracoes de autorizacao
    protected void configure(HttpSecurity http) throws Exception{
        http.authorizeRequests()
        .antMatchers(HttpMethod.GET, "/topicos").permitAll()
        .antMatchers(HttpMethod.GET, "/topicos/*").permitAll()
        .antMatchers(HttpMethod.POST, "/auth").permitAll()
        .anyRequest().authenticated()

        /*Csrf é uma abreviação para cross-site request forgery, 
        que é um tipo de ataque hacker que acontece em aplicações web. 
        Como vamos fazer autenticação via token, automaticamente nossa 
        API está livre desse tipo de ataque. Nós vamos desabilitar isso p
        ara o Spring security não fazer a validação do token do csrf.*/

        .and().csrf().disable()

        /* método para dizer que não queremos usar sessão. Nós passamos 
        como parâmetro essionCreationPolicy.STATELESS. Com isso, aviso
         para o Spring security que no nosso projeto, quando eu fizer 
         autenticação, não é para criar sessão, porque vamos usar token.*/

        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().addFilterBefore(new AutenticacaoViaTokenFilter(tokenService), UsernamePasswordAuthenticationFilter.class);
    }

    //Configuracoes de recursos estaticos(js, css, imagens)
    public void configure(WebSecurity web) throws Exception{
        
    }

    // public static void main(String[] args){
    //     System.out.println(new BCryptPasswordEncoder().encode("123456"));
    // }
}